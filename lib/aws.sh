#!/bin/bash

install_dependencies() {
  dnf install -y jq python3-pip tree
  pip3 install awscli
}

configure_aws_settings() {
  local region="$1"

  aws configure set default.region "${region}"
  aws configure set default.output json
}

set_s3_upload_prefix() {
  if [[ "${SAMPLE_IMAGE}" =~ ^(True|yes)$ ]]; then
    echo "${UUID}/sample-images"
  else
    echo "${UUID}/non-sample-images"
  fi
}

s3_cp() {
  local src="$1"
  local dest="$2"
  local aws_vars="$3"

  aws s3 cp "${src}" "s3://${dest}" --only-show-errors${aws_vars:+ $aws_vars}
}

do_not_import_image() {
  if [[ "${IMPORT_IMAGE}" =~ ^(True|yes)$ ]]; then
    return 1
  else
    return 0
  fi
}

import_snapshot() {
  local s3bucket="$1"
  local s3key="$2"

  aws ec2 import-snapshot \
          --disk-container \
    Format=raw,UserBucket="{S3Bucket=${s3bucket},S3Key=${s3key}}" | \
  jq -r .ImportTaskId
}

describe_import_snapshot_tasks() {
  local import_task_id="$1"

  aws ec2 describe-import-snapshot-tasks --import-task-ids "${import_task_id}"
}

import_status() {
  local import_task_id="$1"

  describe_import_snapshot_tasks "${import_task_id}" | \
  jq -r .ImportSnapshotTasks[0].SnapshotTaskDetail.Status
}

snapshot_id() {
  local import_task_id="$1"

  describe_import_snapshot_tasks "${import_task_id}" | \
  jq -r .ImportSnapshotTasks[0].SnapshotTaskDetail.SnapshotId
}

create_snapshot_tags() {
  local snapshot_id="$1"
  local component="${2:-Automotive}"
  local owner="${3:-A-TEAM}"
  local environment="${4:-Prod}"
  local fedora_group="${5:-ci}"

  aws ec2 create-tags \
          --resources "${snapshot_id}" \
          --tags Key=ServiceComponent,Value="${component}" \
                 Key=ServiceOwner,Value="${owner}" \
                 Key=ServicePhase,Value="${environment}" \
                 Key=FedoraGroup,Value="${fedora_group}"
}

copy_snapshot() {
  local snapshot_id="$1"
  local src_region="$2"
  local dest_region="$3"

  aws ec2 copy-snapshot \
          --source-snapshot-id "${snapshot_id}" \
          --source-region "${src_region}" \
          --region="${dest_region}" | \
  jq -r .SnapshotId
}

snapshot_state() {
  local snapshot_id="$1"

  aws ec2 describe-snapshots \
          --region "${AWS_TF_REGION}" \
          --snapshot-ids "${snapshot_id}" | \
  jq -r .Snapshots[0].State
}

register_image() {
  local image_name="$1"
  local region="$2"
  local arch="$3"
  local root_device="$4"
  local boot_mode="$5"
  local snapshot_id="$6"

  aws ec2 register-image \
          --name "${image_name}" \
          --region "${region}" \
          --architecture "${arch}" \
          --virtualization-type hvm \
          --root-device-name "${root_device}" \
          --ena-support \
          --boot-mode "${boot_mode}" \
          --block-device-mappings "[
   {
       \"DeviceName\": \"/dev/sda1\",
       \"Ebs\": {
           \"SnapshotId\": \"${snapshot_id}\"
       }
   }]" | jq -r .ImageId
}

create_image_tags() {
  local image_id="$1"
  local region="$2"
  local uuid="$3"
  local component="${4:-Artemis}"
  local service="${5:-Artemis}"
  local app_code="${6:-ARR-001}"
  local owner="${7:-TFT}"
  local environment="${8:-Prod}"

  aws ec2 create-tags \
          --resources "$image_id" \
          --region="${region}" \
          --tags Key=ServiceComponent,Value="${component}" \
                 Key=ServiceName,Value="${service}" \
                 Key=AppCode,Value="${app_code}" \
                 Key=ServiceOwner,Value="${owner}" \
                 Key=ServicePhase,Value="${environment}" \
                 Key=PIPELINE_RUN_ID,Value="${uuid}"
}

image_register_info() {
  local image_id="$1"
  local region="$2"

  aws ec2 describe-images \
          --image-ids "${image_id}" \
          --region "${region}"
}

image_register_status() {
  local image_id="${1%%:*}" # string before the :
  local region="${1##*:}"   # string after the :

  image_register_info "$image_id" "${region}" | jq -r .Images[0].State
}

grant_image_permissions() {
  local image_id="$1"
  local region="$2"
  local user_id="$3"

  aws ec2 modify-image-attribute \
          --image-id "${image_id}" \
          --region "${region}" \
          --launch-permission "Add=[{UserId=${user_id}}]"
}

task_progress() {
  local check_function="$1"
  local item_id="$2"
  local stop_state="$3"
  local error_state="$4"

  set +x # Disable the debug during the loop
  until [[ "$state" == "$stop_state" ]]; do
    state=$($check_function "${item_id}")
    if [[ "$state" == "$error_state" ]]; then
      echo "ERROR: state = $error_state"
      exit 1
    fi
    # Send progress dots to stderr to leave clean the stdout for the func return
    echo -n "." >&2
  done
  set -x # Enable the debug again
  echo "$state"
}

publish_ami_info() {
  S3_UPLOAD_PREFIX="${UUID}/sample-images"
  DATA_FILE="AMI_info.json"

  cat <<EOF | tee -a "${DATA_FILE}"
{
  "ami_name": "${IMAGE_KEY}",
  "arch": "${ARCH}",
  "os_version": "${OS_PREFIX}${OS_VERSION}",
  "build_type": "${BUILD_TYPE}",
  "image_name": "${IMAGE_NAME}",
  "image_type": "${IMAGE_TYPE}",
  "UUID": "${UUID}"
}
EOF

  s3_cp "${DATA_FILE}" "${S3_BUCKET_NAME}/${S3_UPLOAD_PREFIX}/"
}

delete_raw_images(){
  local s3bucket="$1"
  local s3upload_prefix="$2"
  local image_key="$3"

  aws s3 rm "s3://${s3bucket}/${s3upload_prefix}/${image_key}.raw"
  aws s3 rm "s3://${s3bucket}/${s3upload_prefix}/${image_key}.json"
}

