#!/bin/bash

install_git() {
  dnf install -y git
}

check_ssl() {
  if [[ "${SSL_VERIFY}" == "false" ]]; then
    export GIT_SSL_NO_VERIFY=true
  fi
}

clone_sample_images_repo() {
  check_ssl
  git clone "${REPO_URL}" "${OSBUILD_DIR}"
  cd "${OSBUILD_DIR}" || exit 1
  git checkout -b upstream "origin/${REVISION}"
}
