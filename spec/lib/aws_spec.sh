Describe 'aws'
  Include ./lib/aws.sh

  Describe 'install dependencies'
    Mock dnf
      echo "dnf $*"
    End
    Mock pip3
      echo "pip3 $*"
    End

    It 'installs jq'
      When call install_dependencies
        The output should include "jq"
    End

    It 'installs awscli'
      When call install_dependencies
        The output should include "awscli"
    End
  End

  Describe 'set_s3_updload_prefix'
    UUID="XXXX"
    Describe 'sample images should use sample-images dir'
      Parameters
        "True"
        "yes"
      End

      It "SAMPLE_IMAGE=$1"
        SAMPLE_IMAGE="$1"
        When call set_s3_upload_prefix
        The output should equal "XXXX/sample-images"
      End
    End

    Describe 'non sample images should use non-sample-images dir'
      Parameters
        "False"
        "no"
      End

      It "SAMPLE_IMAGE=$1"
        SAMPLE_IMAGE="$1"
        When call set_s3_upload_prefix
        The output should equal "XXXX/non-sample-images"
      End
    End
  End

  Describe 'do_not_import'
    Describe 'return 1 when True/yes and 0 when other values'
      Parameters
        "True" 1
        "yes" 1
        "False" 0
        "no" 0
	"" 0 # variable not defined or empty
      End

      It "IMPORT_IMAGE=$1"
        IMPORT_IMAGE="$1"
        When call do_not_import_image
        The status should equal $2
      End
    End
  End

End
