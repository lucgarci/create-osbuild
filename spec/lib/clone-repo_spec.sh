Describe 'clone-repo'
  Include ./lib/clone-repo.sh

  Describe 'install dependencies'
    Mock dnf
      echo "dnf $*"
    End

    It 'installs git'
      When call install_git
        The output should include "git"
    End
  End

  Describe 'check_ssl'
    BeforeEach 'unset GIT_SSL_NO_VERIFY'

    It 'disable ssl to git if SSL_VERIFY is false'
      SSL_VERIFY="false"
      When call check_ssl
        The variable GIT_SSL_NO_VERIFY should equal "true"
    End

    It 'do not disable ssl to git if SSL_VERIFY is true'
      SSL_VERIFY="true"
      When call check_ssl
        The variable GIT_SSL_NO_VERIFY should not be defined
    End
  End

  Describe 'clone_sample_images_repo'
    BeforeEach 'unset GIT_SSL_NO_VERIFY'
    Mock git
      echo "git $*"
    End
    cd() { :; }
    REPO_URL="https://my-org.org/repo.git"
    OSBUILD_DIR="osbuild-manifests"
    REVISION="fake-branch"

    It 'disable ssl to git if SSL_VERIFY is false'
      SSL_VERIFY="false"
      When call clone_sample_images_repo
        The variable GIT_SSL_NO_VERIFY should be defined
        The variable GIT_SSL_NO_VERIFY should equal "true"
	The line 1 of stdout should equal "git clone https://my-org.org/repo.git osbuild-manifests"
	The line 2 of stdout should equal "git checkout -b upstream origin/fake-branch"
    End

    It 'do not disable ssl to git if SSL_VERIFY is true'
      SSL_VERIFY="true"
      When call clone_sample_images_repo
        The variable GIT_SSL_NO_VERIFY should not be defined
	The line 1 of stdout should equal "git clone https://my-org.org/repo.git osbuild-manifests"
	The line 2 of stdout should equal "git checkout -b upstream origin/fake-branch"
    End
  End
End
