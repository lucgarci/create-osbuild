#!/bin/bash

set -euxo pipefail

source lib/clone-repo.sh

OSBUILD_DIR=${OSBUILD_DIR:-osbuild-manifests}

echo "[+] Install Git"
install_git

echo "[+] Remove any previous mpp files"
rm -vfr "${OSBUILD_DIR}"

echo "[+] Clone the repository with the mpp files"
clone_sample_images_repo
echo "[+] Cloned"
