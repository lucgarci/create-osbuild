#!/bin/bash

set -euxo pipefail

source lib/create-osbuild.sh

set_vars

workaound_for_gpg

install_dependencies

set_osbuild_options

cd "${MANIFEST_DIR}"

build_target "${DISK_IMAGE}"

echo "[+] Checking build repo needed? [${TEST_IMAGE}] [${IMAGE_NAME}]"
if [[ "${TEST_IMAGE}" =~ ^(True|yes)$ ]] &&
   [[ "${IMAGE_NAME}" == "qa" ]] && [[ "${IMAGE_TYPE}" == "ostree" ]]; then
  echo "[+] building remote repo"
  build_target "${UPGRADE_REPO_NAME}"
  echo "[+] Moving the generated remote repo"
  mkdir -p "$(dirname "$UPGRADE_REPO_DIR")"
  mv "$UPGRADE_REPO_NAME" "$UPGRADE_REPO_DIR"
fi

echo "[+] Moving the generated image"
mkdir -p "$(dirname "$IMAGE_FILE")"
mv "$DISK_IMAGE" "$IMAGE_FILE"

build_metadata

cleanup

echo "The final image is here: ${IMAGE_FILE}"
echo "Information about image is in ${IMAGE_FILE%.*}.json"
